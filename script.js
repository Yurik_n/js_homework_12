/*
1. Чому для роботи з input не рекомендується використовувати клавіатуру?

1. Можливо не відслідкувати дані, які введені не клавіатурою(зробити, наприклад вставку
    за допомогою миші)
*/

const buttons = document.querySelectorAll(".btn")

document.addEventListener("keyup", (e) => {
    buttons.forEach((btn) => {
        if ((e.code == btn.innerHTML || e.code == `Key${btn.innerHTML}`)) {        
                btn.classList.add("blue")
        } else  {
            btn.classList.remove("blue")          
        }
    });
})



// document.addEventListener("keyup", (e) => {
//     buttons.forEach((btn) => {
//         if (!btn.classList.contains("black-color") && (e.code == btn.innerHTML || e.code == `Key${btn.innerHTML}`)) {        
//                 btn.classList.add("blue-color")
//         } else if (btn.classList.contains("blue-color")) {
//             btn.classList.add("black-color")          
//         }
//     });
// })